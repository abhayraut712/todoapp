import React from "react";

const Form = (props) => {

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!e.target.name.value.trim()) {
      return;
    }
    props.addTask(e.target.name.value, e.target.desc.value, e.target.deadline.value);
  }

  return (
      <form onSubmit={handleSubmit}>
        <h2>
          TODO app
        </h2>
        <label>Task Title : </label>
        <input
          type="text"
          id="new-todo-input"
          name="name"
          required
        />
        <label>Description : </label>
        <input
          type="text"
          id="new-todo-desc"
          name="desc"
        />
        <label>Deadline : </label>
        <input
          type="time"
          id="new-todo-time"
          name="deadline"
          required
        />
        <button type="submit">
          Add
        </button>
        <p>
          <label>*** Note : Please check the checkbox once you have completed your task. ***</label>
        </p>
      </form>
    );
}

export default Form;
