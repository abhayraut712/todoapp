import React from "react";
import DeadlineEval from "./deadlineReminder";

function EditingTemplate(props){
    return (
      <form onSubmit={props.handleSubmit}>
        <div>
          New name for {props.name}
          <input
            id={props.id}
            type="text"
            value={props.newName}
            onChange={props.handleChange}
          />
        </div>
        <div>
  
          <button
            type="button"
            onClick={() => props.setEditing(false)}
          >
            Cancel
          <span>renaming {props.name}</span>
          </button>
          <button type="submit" >
            Save
          <span>new name for {props.name}</span>
          </button>
        </div>
      </form>
    );
  }
  
  
  function ViewTemplate(props){
    return (
      <div style={{ border: "white 2px solid", padding: "5px" }}>
        <div>
          <input
            id={props.id}
            type="checkbox"
            defaultChecked={props.completed}
            onChange={() => props.toggleTaskCompleted(props.id)}
          />
          <label>
            {props.name}
          </label>
          <p>Description : {props.desc}</p>
          <p>Deadline : {props.deadline}</p>
          <div> <DeadlineEval deadline={props.deadline} completed={props.completed} /> </div>
        </div>
        <div >
          <button
            type="button"
            className="btn"
            onClick={() => props.setEditing(true)}
          >
            Edit <span>{props.name}</span>
          </button>
          <button
            type="button"
            onClick={() => props.deleteTask(props.id)}
          >
            Delete <span>{props.name}</span>
          </button>
        </div>
      </div>
    );
  
  }

export {EditingTemplate, ViewTemplate};