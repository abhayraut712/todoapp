import React from "react";

export default function DeadlineEval(props){
    let currentDate = new Date().toLocaleTimeString().split(":");
    let taskDate = props.deadline.split(":");
    const hourInterval = Number(taskDate[0]) - Number(currentDate[0]);
    const minutesInterval = Number(taskDate[1]) - Number(currentDate[1]);
    if (props.completed === true) {
      return (
        <p style={{ color: "green" }}>Completed</p>
      );
    } else if (hourInterval > 1) {
      return (
        // <p>Not Urgent</p>
        null
      );
    } else if (hourInterval === 1) {
      if ((Number(taskDate[1]) + 60) - Number(currentDate[1]) <= 15 && (Number(taskDate[1]) + 60) - Number(currentDate[1]) > 0) {
        return (
          <p style={{ color: "red" }}>Urgent : {60 + minutesInterval} minutes left</p>
        );
      } else {
        return (
          // <p>Not Urgent</p>
          null
        );
      }
    } else if (hourInterval < 0) {
      return (
        <p style={{ color: "yellow" }}>Overdued</p>
      );
    } else {
      if (minutesInterval <= 15 && minutesInterval > 0) {
        return (
          <p style={{ color: "red" }}>Urgent : {minutesInterval} minutes left</p>
        );
      } else if (minutesInterval > 15) {
        return (
          // <p>Not Urgent</p>
          null
        );
      } else {
        return (
          <p style={{ color: "yellow" }}>Overdued</p>
        );
      }
    }
  }
  
  