import React from "react";
import { EditingTemplate, ViewTemplate } from "./templates";

export default class Todo extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isEditing: false,
      newName: '',
      counter: 0,
    }
  }
  setCounter = () => {
    this.setState({
      counter: this.state.counter + 1
    })
  }

  setEditing = (val) => {
    this.setState({
      isEditing: val
    })
  }
  setNewName = (val) => {
    this.setState({
      newName: val
    })
  }

  handleChange = (e) => {
    this.setNewName(e.target.value);
  }

  handleSubmit = (e) => {
    e.preventDefault();
    if (!this.state.newName.trim()) {
      return;
    }
    this.props.editTask(this.props.id, this.state.newName);
    this.setNewName("");
    this.setEditing(false);
  }


  componentDidMount() {
    setInterval(this.setCounter, 1000);
  }


  render() {
    // let newName = this.state.newName;
    let isEditing = this.state.isEditing;

    const editingTemplate = <EditingTemplate handleSubmit={this.handleSubmit}
                                              name={this.props.name}
                                              id={this.props.id}
                                              newName={this.state.newName}
                                              handleChange={this.handleChange}
                                              setEditing={this.setEditing} />

    const viewTemplate = <ViewTemplate id={this.props.id}
                                        completed={this.props.completed}
                                        toggleTaskCompleted={this.props.toggleTaskCompleted}
                                        name={this.props.name}
                                        desc={this.props.desc}
                                        deadline={this.props.deadline}
                                        setEditing={this.setEditing}
                                        deleteTask={this.props.deleteTask} />


    return <li className="todo">{isEditing ? editingTemplate : viewTemplate}</li>;
  }
}