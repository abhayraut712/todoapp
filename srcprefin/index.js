import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';


const DATA = [
  { id: "todo-0", name: "assignment1", completed: true, desc: "This is very much important", deadline: "17:00" },
  { id: "todo-1", name: "assignment2", completed: false, desc: "This can be skipped", deadline: "17:15" },
  { id: "todo-2", name: "assignment3", completed: false, desc: "As you wish", deadline: "00:25" }
];

ReactDOM.render(
  <React.StrictMode>
    <App tasks={DATA} />
  </React.StrictMode>,
  document.getElementById('root')
);
