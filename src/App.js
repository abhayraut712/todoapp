import React from "react";
import Form from "./components/Functional/Form";
import { nanoid } from "nanoid";
import Filter from "./components/Functional/filter";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: this.props.tasks,
      filter: "All",
      search: "",
    };
  }

  setTasks = (val) => {
    this.setState({
      tasks: val,
    });
  };

  setFilter = (val) => {
    this.setState({
      filter: val,
    });
  };

  setFilterDropdown = (e) => {
    this.setState({
      filter: e.target.value,
    });
  };

  addTask = (name, desc, deadline) => {
    const newTask = {
      id: "todo-" + nanoid(),
      name: name,
      desc: desc,
      deadline: deadline,
      completed: false,
    };
    this.setTasks([...this.state.tasks, newTask]);
    console.log(newTask);
  };

  toggleTaskCompleted = (id) => {
    const updatedTasks = this.state.tasks.map((task) => {
      if (id === task.id) {
        return { ...task, completed: !task.completed };
      }
      return task;
    });
    this.setTasks(updatedTasks);
  };

  deleteTask = (id) => {
    const remainingTasks = this.state.tasks.filter((task) => id !== task.id);
    this.setTasks(remainingTasks);
  };

  editTask = (id, newName, desc, deadline) => {
    const editedTaskList = this.state.tasks.map((task) => {
      if (id === task.id) {
        return { ...task, name: newName, desc: desc, deadline: deadline };
      }
      return task;
    });
    this.setTasks(editedTaskList);
  };

  updateSearch = (event) => {
    this.setState({ search: event.target.value.substr(0, 20) });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (!e.target.name.value.trim()) {
      return;
    }
    this.addTask(
      e.target.name.value,
      e.target.desc.value,
      e.target.deadline.value
    );
  };

  render() {
    return (
      <div>
        <Form handleSubmit={this.handleSubmit} name="" deadline="" desc="" />
        <p>
          <label>
            *** Note : Please check the checkbox once you have completed your task. ***
          </label>
        </p>
        <div>
          <div style={{ float: "left", width: "100%" }}>
            <input
              type="text"
              id="new-todo-search"
              name="text"
              value={this.search}
              placeholder="Search.."
              onChange={this.updateSearch}
            />
          </div>

          <Filter
            tasks={this.state.tasks}
            filter={this.state.filter}
            setFilterDropdown={this.setFilterDropdown}
            toggleTaskCompleted={this.toggleTaskCompleted}
            deleteTask={this.deleteTask}
            editTask={this.editTask}
            // tasks={this.state.tasks}
            search={this.state.search}
            updateSearch={this.updateSearch}
            // toggleTaskCompleted={this.toggleTaskCompleted}
            // deleteTask={this.deleteTask}
            // editTask={this.editTask}
            length={this.state.search.length}
          />
        </div>
        <div>
          {/* <Search
            tasks={this.state.tasks}
            search={this.state.search}
            updateSearch={this.updateSearch}
            toggleTaskCompleted={this.toggleTaskCompleted}
            deleteTask={this.deleteTask}
            editTask={this.editTask}
            length={this.state.search.length}
          /> */}
        </div>
      </div>
    );
  }
}

export default App;
