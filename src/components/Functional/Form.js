import React from "react";

const Form = (props) => { 

  return (
      <form onSubmit={props.handleSubmit}>
        <h2>
          TODO app
        </h2>
        <label>Task Title : </label>
        <input
          type="text"
          id="new-todo-input"
          name="name"
          defaultValue={props.name}
          required
        />
        <label>Description : </label>
        <input
          type="text"
          id="new-todo-desc"
          name="desc"
          defaultValue={props.desc}
          required
        />
        <label>Deadline : </label>
        <input
          type="time"
          id="new-todo-time"
          name="deadline"
          defaultValue={props.deadline}
          required
        />
        <button type="submit">
          Submit
        </button>
      </form>
    );
}

export default Form;
