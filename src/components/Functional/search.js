import React from "react";
import Todo from "../Class/Todo";

function Search(props) {
  let searchList = props.taskList.filter((item) => {
    return (
      item.props.name.toLowerCase().indexOf(props.search.toLowerCase()) !== -1
    );
  });

  let newSearchlist = [];
  for (let z = 0; z < searchList.length; z++) {
    newSearchlist.push(searchList[z].props);
  }
  
  let list = "";
  if (props.length > 0) {
    list = newSearchlist.map((task) => (
      <Todo
        id={task.id}
        name={task.name}
        completed={task.completed}
        key={task.id}
        desc={task.desc}
        deadline={task.deadline}
        toggleTaskCompleted={props.toggleTaskCompleted}
        deleteTask={props.deleteTask}
        editTask={props.editTask}
      />
    ));
  }

  return (
    <div>
      <div style={{ float: "left", width: "100%" }}>
        <div>
          <ul>
            {list}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Search;
