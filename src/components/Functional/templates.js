import React from "react";
import DeadlineEval from "./deadlineReminder";
import Form from "./Form";

function EditingTemplate(props) {
  const handleSubmit = (e) => {
     e.preventDefault();
    if (!e.target.name.value.trim()) {
      return;
    }
    props.editTask(props.id, e.target.name.value ,e.target.desc.value, e.target.deadline.value);
    props.setEditing(false);
  }
  return (
    <div>

      <Form handleSubmit={handleSubmit} name={props.name} deadline={props.deadline} desc={props.desc}/>

      <button type="button" onClick={() => props.setEditing(false)}>
        Cancel
          <span>renaming {props.name}</span>
      </button>
    </div>
  );
}

function ViewTemplate(props) {
  return (
    <div style={{ border: "white 2px solid", padding: "5px" }}>
      <div>
        <input
          id={props.id}
          type="checkbox"
          defaultChecked={props.completed}
          onChange={() => props.toggleTaskCompleted(props.id)}
        />
        <label>{props.name}</label>
        <p>Description : {props.desc}</p>
        <p>Deadline : {props.deadline}</p>
        <div>
          {" "}
          <DeadlineEval
            deadline={props.deadline}
            completed={props.completed}
          />{" "}
        </div>
      </div>
      <div>
        <button
          type="button"
          className="btn"
          onClick={() => props.setEditing(true)}
        >
          Edit <span>{props.name}</span>
        </button>
        <button type="button" onClick={() => props.deleteTask(props.id)}>
          Delete <span>{props.name}</span>
        </button>
      </div>
    </div>
  );
}

export { EditingTemplate, ViewTemplate };
