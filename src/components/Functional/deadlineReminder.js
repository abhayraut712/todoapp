import React from "react";

export default function DeadlineEval(props) {
  let hours = new Date();
  let currentDate = [hours.getHours(), hours.getMinutes()];
  let taskDate = props.deadline.split(":");
  const hourInterval = Number(taskDate[0]) - Number(currentDate[0]);
  const minutesInterval = Number(taskDate[1]) - Number(currentDate[1]);
  if (props.completed === true) {
    return <p style={{ color: "green" }}>Completed</p>;
  } 
  else if (hourInterval === 1) {
      if (
        Number(taskDate[1]) + 60 - Number(currentDate[1]) <= 15 &&
        Number(taskDate[1]) + 60 - Number(currentDate[1]) > 0
      ) {
        return (
          <p style={{ color: "red" }}>
            Urgent : {60 + minutesInterval} minutes left
          </p>
        );
      }
  } 
  else if (hourInterval < 0) {
      return <p style={{ color: "yellow" }}>Overdued</p>;
  } 
  else if (hourInterval === 0) {
      if (minutesInterval <= 15 && minutesInterval > 0) {
        return ( <p style={{ color: "red" }}>Urgent : {minutesInterval} minutes left</p>);
      } else if(minutesInterval<= 0) {
          return (<p style={{ color: "yellow" }}>Overdued</p>);
      }
  }
  return null;
}
