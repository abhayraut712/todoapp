import React from "react";
import Todo from "../Class/Todo";
import FilterButton from "./FilterButton";
import Search from "./search";

const FILTER_MAP = {
  All: () => true,
  Active: (task) => !task.completed,
  Completed: (task) => task.completed,
};

const FILTER_NAMES = Object.keys(FILTER_MAP);

function Filter(props) {
  const taskList = props.tasks
    .filter(FILTER_MAP[props.filter])
    .map((task) => (
      <Todo
        id={task.id}
        name={task.name}
        desc={task.desc}
        deadline={task.deadline}
        completed={task.completed}
        key={task.id}
        toggleTaskCompleted={props.toggleTaskCompleted}
        deleteTask={props.deleteTask}
        editTask={props.editTask}
      />
    ));

  const filterList = FILTER_NAMES.map((name) => (
    <FilterButton key={name} name={name} />
  ));

  if (props.length > 0) {
    return (
      <div>
        <div style={{width: "100%" }}>
          <select value={props.filter} onChange={props.setFilterDropdown}>
            {filterList}
          </select>
        </div>
        <div>
          <Search
            taskList={taskList}
            tasks={props.tasks}
            search={props.search}
            updateSearch={props.updateSearch}
            toggleTaskCompleted={props.toggleTaskCompleted}
            deleteTask={props.deleteTask}
            editTask={props.editTask}
            length={props.search.length}
          />
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <div style={{width: "100%" }}>
          <select value={props.filter} onChange={props.setFilterDropdown}>
            {filterList}
          </select>
          <ul>{taskList}</ul>
        </div>
      </div>
    );
  }
}

export default Filter;
