import React from "react";
import { EditingTemplate, ViewTemplate } from "../Functional/templates";

export default class Todo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditing: false,
      counter: 0,
    };
  }
  setCounter = () => {
    this.setState({
      counter: this.state.counter + 1,
    });
  };

  setEditing = (val) => {
    this.setState({
      isEditing: val,
    });
  };

  componentDidMount() {
    setInterval(this.setCounter, 1000);
  }

  render() {
    let isEditing = this.state.isEditing;

    const editingTemplate = (
      <EditingTemplate
        name={this.props.name}
        id={this.props.id}
        deadline={this.props.deadline}
        desc={this.props.desc}
        handleChange={this.handleChange}
        setEditing={this.setEditing}
        editTask={this.props.editTask}
      />
    );

    const viewTemplate = (
      <ViewTemplate
        id={this.props.id}
        completed={this.props.completed}
        toggleTaskCompleted={this.props.toggleTaskCompleted}
        name={this.props.name}
        desc={this.props.desc}
        deadline={this.props.deadline}
        setEditing={this.setEditing}
        deleteTask={this.props.deleteTask}
      />
    );

    return (
      <li className="todo">{isEditing ? editingTemplate : viewTemplate}</li>
    );
  }
}
