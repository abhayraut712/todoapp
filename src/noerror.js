import React from "react";
import Form from "./components/Form";
import FilterButton from "./components/FilterButton";
import Todo from "./components/Todo";
import { nanoid } from "nanoid";


const FILTER_MAP = {
  All: () => true,
  Active: task => !task.completed,
  Completed: task => task.completed
};

const FILTER_NAMES = Object.keys(FILTER_MAP);

class App extends React.Component {

constructor(props){
  super(props);
  this.state = {
    tasks : this.props.tasks,
    filter : 'All',
    search :''
  }
}

setTasks = (val) => {
  this.setState({
    tasks:val,
  });
}
setFilter = (val) => {
  this.setState({
    filter:val,
  })
}
addTask=(name,desc,deadline)=> {
  const newTask = { id: "todo-" + nanoid(), name: name,desc:desc,deadline:deadline, completed: false };
  this.setTasks([...this.state.tasks, newTask]);
  console.log(newTask);
}
toggleTaskCompleted=(id)=> {
  const updatedTasks = this.state.tasks.map(task => {
    // if this task has the same ID as the edited task
    if (id === task.id) {
      // use object spread to make a new obkect
      // whose `completed` prop has been inverted
      return {...task, completed: !task.completed}
    }
    return task;
  });
  this.setTasks(updatedTasks);
}


deleteTask=(id)=> {
  const remainingTasks = this.state.tasks.filter(task => id !== task.id);
  this.setTasks(remainingTasks);
}


editTask=(id, newName)=> {
  const editedTaskList = this.state.tasks.map(task => {
  // if this task has the same ID as the edited task
    if (id === task.id) {
      //
      return {...task, name: newName}
    }
    return task;
  });
  this.setTasks(editedTaskList);
}
updateSearch=(event)=>{
  this.setState({search:event.target.value.substr(0,20)});
}

render(){

  const taskList = this.state.tasks
  .filter(FILTER_MAP[this.state.filter])
  .map(task => (
    <Todo
      id={task.id}
      name={task.name}
      desc={task.desc}
      deadline={task.deadline}
      completed={task.completed}
      key={task.id}
      toggleTaskCompleted={this.toggleTaskCompleted}
      deleteTask={this.deleteTask}
      editTask={this.editTask}
    />
  ));

  const filterList = FILTER_NAMES.map(name => (
    <FilterButton
      key={name}
      name={name}
      isPressed={name === this.state.filter}
      setFilter={this.setFilter}
    />
  ));

  let searchList = this.state.tasks.filter(((item)=>{
    return item.name.toLowerCase().indexOf(this.state.search)!==-1;
  }));
  let list = ""
   if(this.state.search.length>0){
     list= searchList.map((task)=><Todo
      id={task.id}
      name={task.name}
      completed={task.completed}
      key={task.id}
      desc={task.desc}
      deadline={task.deadline}
      toggleTaskCompleted={this.toggleTaskCompleted}
      deleteTask={this.deleteTask}
      editTask={this.editTask}
    />)
   }
  return (
    <div>
      <Form addTask={this.addTask} />
      <div style={{width: '100% '}}>
       <div style={{float:'left', width: '50%'}}>
       <input
        type="text"
        id="new-todo-input"
        name="text"
        value={this.state.search}
        placeholder="Search.."
        onChange={this.updateSearch}
      />
     {list}
      </div>
      <div style={{float:'left', width: '50%'}}>
      <div>
        <selected>
        {filterList}
        </selected>
      </div>
      <ul>
        {taskList}
      </ul>
    </div>
    </div>
    </div>
  );
}
}

export default App;
